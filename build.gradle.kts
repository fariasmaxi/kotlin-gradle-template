import org.gradle.jvm.tasks.Jar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

group = "com.fariasmaxi.kotlin-gradle-template"
version = "1.0-SNAPSHOT"
val fatJarProjectName = "${project.name}-fat"


plugins {
	kotlin("jvm") version "1.3.31"
}

dependencies {
	compile("org.jetbrains.kotlin:kotlin-stdlib:1.3.31")
	testImplementation("junit:junit:4.11")
	testImplementation("org.amshove.kluent:kluent:1.47")
}

repositories {
	jcenter()
}

tasks.withType<KotlinCompile> {
	kotlinOptions.jvmTarget = "1.8"
}

val fatJar = task("fatJar", type = Jar::class) {
	baseName = fatJarProjectName
	manifest {
		attributes["Main-Class"] = "com.fariasmaxi.Main"
	}
	from(configurations.runtime.map {
		if (it.isDirectory) it
		else zipTree(it)
	})
	with(tasks["jar"] as CopySpec)
}

tasks {
	"build" {
		dependsOn(fatJar)
	}
}