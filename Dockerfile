# latest oracle openjdk is the basis
FROM openjdk:oracle

# copy jar file into container image under app directory
COPY build/libs/kotlin-gradle-template-fat-1.0-SNAPSHOT.jar app/app.jar

# expose server port accept connections
EXPOSE 8080

# start application
CMD ["java", "-jar", "app/app.jar"]