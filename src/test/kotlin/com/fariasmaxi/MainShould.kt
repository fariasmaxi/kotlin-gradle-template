package com.fariasmaxi

import org.amshove.kluent.`should equal`
import org.junit.Test

class MainShould {
	@Test
	fun `retrieve hello world message`() {
		val main = Main()
		main.helloWorld() `should equal` "Hello world!"
	}
}