package com.fariasmaxi

class Main {


	companion object {
		@JvmStatic
		fun main(args : Array<String>){
			println(" _  ______ _______ _      _____ _   _ \n" +
					" | |/ / __ \\__   __| |    |_   _| \\ | |\n" +
					" | ' / |  | | | |  | |      | | |  \\| |\n" +
					" |  <| |  | | | |  | |      | | | . ` |\n" +
					" | . \\ |__| | | |  | |____ _| |_| |\\  |\n" +
					" |_|\\_\\____/  |_|  |______|_____|_| \\_|\n" +
					"                                       ")
		}
	}

	fun helloWorld(): String {
		return "Hello world!"
	}
}